#
# Copyright (C) 2018-2019 The Google Pixel3ROM Project
# Copyright (C) 2020 Raphielscape LLC. and Haruka LLC.
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := WallpapersBReel2020
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := WallpapersBReel2020.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_PRODUCT_MODULE := true
LOCAL_MODULE_CLASS := APPS
LOCAL_REQUIRED_MODULES := libgdx
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
include $(BUILD_PREBUILT)

LIBGDX_SYMLINK := $(TARGET_OUT_PRODUCT)/app/WallpapersBReel2020/lib/arm64/libgdx.so
$(LIBGDX_SYMLINK): $(LOCAL_INSTALLED_MODULE)
	@echo "libgdx link: $@"
	@mkdir -p $(dir $@)
	@rm -rf $@
	$(hide) ln -sf /product/lib64/libgdx.so $@

ALL_DEFAULT_INSTALLED_MODULES += $(LIBGDX_SYMLINK)
